# README #

### What is this repository for? ###

This discourse plugin is used to configure x-frame-options

* Attribution:  
    * plugin for x-frame-options https://meta.discourse.org/t/x-frame-origin-embed-error/35344/12?u=rchu
    * [Link to git repo](https://github.com/somoza/discourse-xorigin/blob/master/plugin.rb)

### How to install plugins? ###

https://meta.discourse.org/t/install-plugins-in-discourse/19157/67
